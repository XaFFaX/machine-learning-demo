# Machine Learning Demo

Demo project to show potential usages of Spark Structured Streaming and Spark Machine Learning Library.

## Running

To run the examples do:

`> sbt runMain MLDemo.MlModelCreationDemo`
To run application that creates machine learning model. Necessary to run next application.
Training data is taken from MLdata/train folder under resources.
Models are saved to MLdata folder.

`> sbt runMain MLDemo.StreamingMLEvaluation`
Application processes in real time data that is evaluated by the models created in previous step.
Data is taken from MLdata/eval folder under resources.
Use examples contained there to see data scheme.

`> sbt runMain MLDemo.StructuredStreamingDemo`
Streamed processing of data in form of a JSON file. Data is taken from json folder under resources.
See example contained there for data format.