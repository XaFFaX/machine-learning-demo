package MLDemo

import MLDemo.MLModelCreationDemo.resources
import MLDemo.utils.SessionWrapper.sparkSession
import org.apache.spark.ml.PipelineModel
import org.apache.spark.sql.types.StructType

object StreamingMLEvaluation extends App {

  val eval_data_schema = new StructType()
    .add("age", "integer")
    .add("workclass", "string")
    .add("fnlwgt", "string")
    .add("education", "string")
    .add("education-num", "integer")
    .add("marital-status", "string")
    .add("occupation", "string")
    .add("relationship", "string")
    .add("race", "string")
    .add("sex", "string")
    .add("capital-gain", "integer")
    .add("capital-loss", "integer")
    .add("hours-per-week", "integer")
    .add("native-country", "string")
    .add("earnings", "string")

  //streaming read of CSV data
  val df_eval = sparkSession.readStream
    .option("ignoreLeadingWhiteSpace", "true")
    .schema(eval_data_schema)
    .csv("src/main/resources/MLdata/eval")
    .drop("fnlwgt")
    .na.drop("any")

  import org.apache.spark.sql.functions._
  import sparkSession.implicits._

  //load model
  //apply to evaluation data
  //calculate true prediction rate
  //output to console
  val streamingEval = for (m <- Range(0, 4))
    yield PipelineModel.load(resources + "/MLdata/model" + m)
      .transform(df_eval)

  val evalOut = streamingEval.map(
    _.groupBy('label)
      .agg(
        (sum(when('prediction === 'label, 1)) / count('label)).alias("true prediction rate"),
        count('label).alias("count")
      )
  )

  evalOut.foreach(
    _.writeStream
      .outputMode("complete")
      .format("console")
      .option("truncate", "false")
      .start()
  )

  streamingEval.foreach(
    _.writeStream
      .outputMode("append")
      .format("console")
      .option("truncate", "false")
      .start()
  )

  //wait until killed
  sparkSession.streams.awaitAnyTermination()
}
