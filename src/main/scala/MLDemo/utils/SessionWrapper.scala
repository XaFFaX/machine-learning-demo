package MLDemo.utils

import org.apache.spark.sql.SparkSession

object SessionWrapper {
  lazy val sparkSession: SparkSession = SparkSession.builder().master("local[*]").appName("ML Demo").getOrCreate()
  sparkSession.sparkContext.setLogLevel("ERROR")
  //sparkSession.streams.addListener(StreamingQueryListenerImpl)
}
