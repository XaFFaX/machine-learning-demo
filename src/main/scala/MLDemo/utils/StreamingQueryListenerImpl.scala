package MLDemo.utils

import org.apache.spark.sql.streaming.StreamingQueryListener

object StreamingQueryListenerImpl extends StreamingQueryListener{
  override def onQueryStarted(event: StreamingQueryListener.QueryStartedEvent): Unit = println("Query started: " + event.id)

  override def onQueryProgress(event: StreamingQueryListener.QueryProgressEvent): Unit = println("Query progress: " + event.progress)

  override def onQueryTerminated(event: StreamingQueryListener.QueryTerminatedEvent): Unit = println("Query terminated: " + event.id)
}
