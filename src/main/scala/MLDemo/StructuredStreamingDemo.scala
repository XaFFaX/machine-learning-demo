package MLDemo

import MLDemo.utils.SessionWrapper._
import org.apache.spark.sql.functions.current_timestamp
import org.apache.spark.sql.types.StructType

object StructuredStreamingDemo extends App {

  val resources = "src/main/resources/json"

  //{"id":1,"first_name":"Grier","last_name":"Owbridge","email":"gowbridge0@hc360.com","gender":"Female","ip_address":"49.60.191.138"}

  val jsonSchema = new StructType()
    .add("id", "integer")
    .add("first_name", "string")
    .add("last_name", "string")
    .add("email", "string")
    .add("gender", "string")
    .add("ip_address", "string")

  //streaming read of JSON data with addition of current timestamp column
  val users = sparkSession.readStream
    .schema(jsonSchema)
    .json(resources)
    .withColumn("timestamp", current_timestamp())

  import org.apache.spark.sql.functions._
  import sparkSession.implicits._

  //find emails with .pl domain
  val howManyPlDomains = users.filter($"email".endsWith(".pl")).groupBy("email").count()
  //calculate average length of last name
  val avgLastNameLength = users.agg(mean(length(col("last_name"))).as("Avg last name length"))
  //number of same names over 20s window with 10s slide duration
  val firstNameWindowCount = users.groupBy(window($"timestamp", "20 seconds", "10 seconds"), $"first_name").count()
  //same as above but global
  val firstNameCount = users.groupBy($"first_name").count()

  //group streams
  val allStreams = Array(howManyPlDomains, avgLastNameLength, firstNameCount, firstNameWindowCount)

  //output to console
  users.writeStream
    .outputMode("append")
    .format("console")
    .option("truncate", "false")
    .start()

  for (a <- allStreams) {
    a.writeStream
      .outputMode("complete")
      .format("console")
      .option("truncate", "false")
      .start()
  }

  //wait forever
  sparkSession.streams.awaitAnyTermination()
}
