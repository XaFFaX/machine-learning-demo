package MLDemo

import MLDemo.utils.SessionWrapper._
import org.apache.spark.ml.classification._
import org.apache.spark.ml.{Pipeline, feature}

object MLModelCreationDemo {

  val resources = "src/main/resources"

  def main(args: Array[String]): Unit = {

    val col_names = Array("age", "workclass", "fnlwgt", "education", "education-num", "marital-status", "occupation",
      "relationship", "race", "sex", "capital-gain", "capital-loss", "hours-per-week",
      "native-country", "earnings")

    //load CSV data
    val raw_df = sparkSession.read.option("ignoreLeadingWhiteSpace", "true")
      .option("inferSchema", "true")
      .csv(resources + "/MLdata/train/adultTrain.data")

    //drop unnecessary rows
    val trainDf = raw_df.toDF(col_names: _*)
      .drop("fnlwgt")
      .na.drop("any")

    println("Example records:")
    trainDf.show(3, 20, true)

    //create index for earnings column
    val indexer = new feature.StringIndexer().setInputCol("earnings").setOutputCol("label")

    //filters out all string columns
    val num_cols = trainDf.dtypes.filter(_._2 != "StringType").map(_._1)
    println("num_cols")
    println("===========")
    num_cols.foreach(println(_))

    //get all categ columns that are string type and are not earnings
    val categ_cols = trainDf.dtypes.filter(x => x._2 == "StringType" & x._1 != "earnings").map(_._1)
    println("\ncateg_cols")
    println("===========")
    categ_cols.foreach(println(_))

    //add _idx to column names
    val categ_cols_idx = categ_cols.map(_ + "_idx")
    println("\ncateg_cols_idx")
    println("===========")
    categ_cols_idx.foreach(println(_))

    //add _vect to column names
    val categ_cols_vect = categ_cols.map(_ + "_vect")
    println("\ncateg_cols_vect")
    println("===========")
    categ_cols_vect.foreach(println(_))

    //create indexes for categ cols
    val indexes = for ((o, n) <- categ_cols.zip(categ_cols_idx))
      yield new feature.StringIndexer().setInputCol(o).setOutputCol(n)

    //define one-hot encoder for indexes
    val OHencoder = new feature.OneHotEncoderEstimator().setInputCols(categ_cols_idx).setOutputCols(categ_cols_vect)

    //combine numerical columns with categ vector columns
    val vectAssembler = new feature.VectorAssembler()
      .setInputCols(num_cols ++ categ_cols_vect)
      .setOutputCol("featuresRaw")

    //define scaler and scale the data
    val scaler = new feature.StandardScaler().setInputCol("featuresRaw").setOutputCol("features")

    val estimators = Array(new GBTClassifier(), new LinearSVC(), new LogisticRegression(), new RandomForestClassifier())

    estimators.foreach(_
      .setLabelCol("label")
      .setFeaturesCol("features"))

    val pipelines = for (e <- estimators)
      yield new Pipeline().setStages(indexes ++ Array(indexer, OHencoder, vectAssembler, scaler, e))

    val pipelineModels = pipelines.map(_.fit(trainDf))

    //save results
    for ((p, i) <- pipelineModels.zipWithIndex)
      p.write
        .overwrite()
        .save(resources + "/MLdata/model" + i)
  }
}
